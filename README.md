Using modern rendering technology that supports a bazillion lights, 
dynamic shadows and a day-night cycle(*), physics and destructible environment, 
Pixplatter is going to blow your mind into a pixelly red mess!


(*) all rendered in glorious 32×32 resolution

The original game was done in a week by myself for the lowrezjam at GameJolt.
The entyr page can be found [here](https://gamejolt.com/games/pixplatter/27159)

After the jam, some more work was done to improve the gameplay and remove the
low rez filter