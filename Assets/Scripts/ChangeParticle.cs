﻿using UnityEngine;
using System.Collections;

public class ChangeParticle : MonoBehaviour {
	void Awake() {
		var ps = GetComponent<ParticleSystem>();
		ps.maxParticles /= (4 - QualitySettings.GetQualityLevel());
		ps.startLifetime /= (4 - QualitySettings.GetQualityLevel());
		Destroy(this);
	}
}
