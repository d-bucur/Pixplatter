﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class BounceText : MonoBehaviour {
	public float moveTime = 0.75f;
	public float readTime = 1f;
	public float xDifference = -3.9f;

	// Use this for initialization
	void Start () {
		var seq = new Sequence();
		seq.AppendInterval(readTime);
		seq.Append(HOTween.Punch (transform, moveTime, new TweenParms()
		                          .Prop("localPosition", new Vector3(xDifference,0,0), true)));
		seq.AppendInterval(readTime);
		seq.Append(HOTween.Punch (transform, moveTime, new TweenParms()
		                          .Prop("localPosition", new Vector3(-xDifference,0,0), true)));
		seq.loops = -1;
		seq.loopType = LoopType.Restart;
		seq.Play();
	}
}
