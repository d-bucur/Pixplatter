﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class CamAnimation : MonoBehaviour {
	public float baseShake = -10;
	void Awake() { 
		Globals.camAnimation = this;
		HOTween.Init(false, false, true);
		HOTween.EnableOverwriteManager(false);
	}

	public void shakeCam(float intensity) {
		transform.localEulerAngles = Vector3.zero;
		HOTween.Shake(transform, 1f, "localEulerAngles", new Vector3(baseShake*intensity,0,0));
	}
}
