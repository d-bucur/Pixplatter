﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {
	public float speed;
	public bool worldRotation = false;
	
	// Update is called once per frame
	void FixedUpdate () {
		if (worldRotation)
			transform.Rotate(0f, speed, 0f);
			//transform.Rotate(Vector3.up, speed, Space.World);
		else
			transform.RotateAround(transform.position, transform.forward, speed);
	}
}
