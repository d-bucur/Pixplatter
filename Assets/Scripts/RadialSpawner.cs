﻿using UnityEngine;
using System.Collections;

public class RadialSpawner : MonoBehaviour {
	public float deltaTime;
	public float distance;
	public GameObject item;

	// Use this for initialization
	void Start () {
		StartCoroutine(spawn());
	}

	IEnumerator spawn() {
		yield return new WaitForSeconds(deltaTime);
		var v = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized;
		v *= Random.Range(0f, 1f);
		var go = Instantiate(item, v * distance + item.transform.position, Quaternion.identity);
		StartCoroutine(spawn());
	}
}
