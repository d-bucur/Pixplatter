﻿using UnityEngine;
using System.Collections;
using ObjectExtensions;

public class AutoDestroy : MonoBehaviour {
	public float aliveTime;
	public float destroyTime;

	// Use this for initialization
	void Start () {
		StartCoroutine(timed());
	}

	IEnumerator timed() {
		yield return new WaitForSeconds(aliveTime);
		gameObject.animatedDestroy(destroyTime);
	}
}
