﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PixellationEffect : ImageEffectBase {
	public int width, height;
	public bool postProcess = true;

	void Awake() {
		Globals.camera = camera;
	}

	void OnGUI() {
		if (Event.current.type.Equals(EventType.Repaint) && camera.targetTexture)
			Graphics.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), camera.targetTexture);
	}

	void OnRenderImage (RenderTexture source, RenderTexture destination) {
		if (postProcess) {
			RenderTexture scaleBuffer = RenderTexture.GetTemporary(width, height);
			scaleBuffer.filterMode = FilterMode.Point;
			//accumTexture.MarkRestoreExpected();
			Graphics.Blit(source, scaleBuffer);
			Graphics.Blit(scaleBuffer, destination);
			RenderTexture.ReleaseTemporary(scaleBuffer);
		}
		else {
			Graphics.Blit(source, destination);
		}
	}
}
