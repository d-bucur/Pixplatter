﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class HUDButton : MonoBehaviour {
	public string clickMethod;
	public void clicked() {
		gameObject.SendMessage(clickMethod);
	}

	public IEnumerator startGame() {
		moveCam(7.845579f);
		yield return new WaitForSeconds(5f);
		Application.LoadLevel(Application.loadedLevel+1);
	}

	public void credits(){
		moveCam(17.93135f);
	}

	public void goToMain() {
		moveCam(13.21199f);
	}

	void moveCam(float y) {
		var p = Globals.camera.transform.position;
		p.y = y;
		HOTween.Punch(Globals.camera.transform, 1f, "position", p);
	}
}
