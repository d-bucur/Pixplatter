﻿using UnityEngine;
using System.Collections;

public class SendProxyMessages : MonoBehaviour {
	void OnTriggerEnter(Collider coll) {
		var reactScript = (ReactToPlayer)coll.GetComponent(typeof(ReactToPlayer));
		if (reactScript != null)
			reactScript.enterRange();
		/*else 
			print (coll.gameObject.name + " has no ReactToPlayer interface");*/
	}
	
	void OnTriggerExit(Collider coll) {
		var reactScript = (ReactToPlayer)coll.GetComponent(typeof(ReactToPlayer));
		if (reactScript != null)
			reactScript.exitRange();
	}
}
