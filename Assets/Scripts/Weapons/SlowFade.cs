﻿using UnityEngine;
using System.Collections;

public class SlowFade : MonoBehaviour {
	public float timeToFade=1f;

	public void fadeOut() {
		transform.parent = null;
		foreach (var ps in GetComponentsInChildren<ParticleSystem>()) {
			ps.enableEmission = false;
		}
		Destroy(gameObject, timeToFade);
	}
}