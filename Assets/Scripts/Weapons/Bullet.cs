﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;
using System;

public class Bullet : MonoBehaviour {
	public float radius = 0.5f;
	public GameObject explosion;
	public float deathAnimationTime = 1f;
	public int penetrations = 1;
	public float autoDestructTimer = 5f;
	public bool damageCreator = false;
	public float pushbackFactor = 40f;
	public float upwardsAngle = 0f;
	public int environmentPenetrationPenalty = 1;
	GameObject createdBy = null;
	int layerMask = 1<<8 | 1<<9 | 1<<10;

	Vector3 lastPos;

	public float damageAmt;

	void Awake() {
		HOTween.Init(true, false, true);
		HOTween.EnableOverwriteManager();
		HOTween.warningLevel = WarningLevel.Important;
	}

	void Start() {
		StartCoroutine ("autoDestroy");
		lastPos = transform.position;
	}

	IEnumerator autoDestroy() {
		yield return new WaitForSeconds(autoDestructTimer);
		die();
	}

	public void initBullet(GameObject createdBy, float damage) {
		this.createdBy = createdBy;
		this.damageAmt = damage;
	}

	void FixedUpdate() {
		if (createdBy == null) return;	// return if no creator yet
		RaycastHit[] hits;
		var dir = transform.position - lastPos;
		hits = Physics.SphereCastAll(lastPos, radius, dir, dir.magnitude, layerMask);
		if (hits != null) {
			Array.Sort<RaycastHit>(hits, (a, b) => a.distance.CompareTo(b.distance));
			foreach (var hit in hits) {
				hitSomething(hit.collider);
			}
		}
		lastPos = transform.position;
	}

	void hitSomething(Collider coll) {
		if (penetrations <= 0) return;
		if (coll.gameObject == createdBy) return;
		var c = coll.GetComponentInParent<Damageable>();
		if (c) {
			var dir = (transform.position - lastPos);
			dir.y = 0;
			dir = Quaternion.Euler(upwardsAngle, 0, 0) * dir.normalized;
			dir.y = Mathf.Abs(dir.y);
			c.damage(damageAmt, transform.rotation, dir, damageAmt * pushbackFactor * 2f);
		}
		if (coll.gameObject.layer == 10)
			penetrations -= environmentPenetrationPenalty;
		else
			penetrations--;


		var r = coll.GetComponentInParent<Rigidbody>();
		if ((coll.gameObject.layer == 10 || coll.gameObject.layer == 9) && r) {
			var dir = (transform.position - lastPos);
			r.AddForce(damageAmt * dir.normalized, ForceMode.Impulse);
			r.AddExplosionForce(damageAmt*pushbackFactor, lastPos, 1000f);
		}

		if (penetrations <= 0) {
			explode();
			die();
		}
	}

	void explode() {
		if (explosion)
			Instantiate(explosion, transform.position, Quaternion.identity);
	}

	void die() {
		// TODO gets called many times. disabled HOtween warnings because of this
		StopCoroutine("autoDestroy");
		var v = transform.Find("Sphere");
		if (v) Destroy(v.gameObject);
		foreach(var slow in GetComponentsInChildren<SlowFade>()) {
			slow.fadeOut();
		}
		Destroy(gameObject);
	}
}
