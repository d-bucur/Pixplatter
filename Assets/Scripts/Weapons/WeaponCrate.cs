﻿using UnityEngine;
using System.Collections;
using ObjectExtensions;

public class WeaponCrate : MonoBehaviour {
	void OnCollisionEnter(Collision other) {
		getRandomWeapon(other.collider);
	}

	void OnTriggerEnter(Collider other) {
		getRandomWeapon(other);
	}

	void getRandomWeapon(Collider other) {
		if (other.GetComponentInParent<Player>()) {
			print("Got WEapon");
			var name = WeaponController.singleton.addAmmo();
			Helper.createFloatingText(transform.position, name);
			Destroy(collider);
			gameObject.animatedDestroy(0.1f);
		}
	}
}
