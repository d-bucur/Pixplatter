﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class WeaponController : MonoBehaviour {
	public bool globalWeaponCooldown = false;
	public static WeaponController singleton;
	private int selectedWeapon = 0;
	private List<Weapon> weapons;
	public AudioClip changeSound;

	public bool canFire {
		get {
			return _canFire;
		}
	}
	bool _canFire = true;

	void Awake() {
		singleton = this;
	}

	public void Start() {
		weapons = new List<Weapon>(GetComponentsInChildren<Weapon>());
		getWeapon (Random.Range(0,weapons.Count-1));
	}
	
	public void fire() {
		weapons[selectedWeapon].fire();
		Globals.ammo = weapons[selectedWeapon].ammo;
	}
	
	private void disableOtherWeapons() {
		foreach (var w in weapons) {
			if (w != weapons[selectedWeapon]) {
				w.gameObject.SetActive(false);
				Cursor.singleton.fade(true);
			}
			else
				w.gameObject.SetActive(true);
		}
	}
	
	public void getWeapon(bool next) {
		getWeapon(selectedWeapon + (next ? +1 : -1));
	}

	public void getWeapon(int i) {
		Helper.playSound(changeSound);
		selectedWeapon = (int)Mathf.Repeat(i, weapons.Count);
		Globals.weaponName = weapons[selectedWeapon].gameObject.name;
		Globals.ammo = weapons[selectedWeapon].ammo;
		disableOtherWeapons();
	}

	void Update() {
#if !UNITY_ANDROID
		if(Input.GetMouseButton(0)) {
			fire ();
		}

		// change weapon with scroll wheel
		var scroll = Input.GetAxisRaw("Mouse ScrollWheel");
		if (scroll < 0) getWeapon(true);
		else if (scroll > 0) getWeapon(false);

		// change weapon with number keys
		foreach (var i in Enumerable.Range(0, 10)) {
			if (Input.GetKeyDown(i.ToString())) {
				getWeapon(i - 1);
			}
		}
#endif
	}

	public void disableShooting(float time) {
		StartCoroutine(disableShootingCoroutine(time));
	}

	IEnumerator disableShootingCoroutine(float time) {
		if (globalWeaponCooldown)
			_canFire = false;
		if (weapons[selectedWeapon].cursorBounces)
			Cursor.singleton.fade(false);
		yield return new WaitForSeconds(time);
		if (globalWeaponCooldown)
			_canFire = true;
		Cursor.singleton.fade(true);
	}

	public string addAmmo() {
		var i = Random.Range(0, weapons.Count);
		weapons[i].ammo += weapons[i].ammoPickup;
		return weapons[i].name + " +" + weapons[i].ammoPickup;
	}
}