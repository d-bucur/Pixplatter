﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Weapon : MonoBehaviour {
	private int _ammo;
	public int ammo {
		get {
			return _ammo;
		}
		set {
			_ammo = value;
			/*if (value < 0)
				throw new System.Exception("Ammo can't be negative");*/
		}
	}
	public int initialAmmo;
	public GameObject bulletGO;
	public float damagerPerBullet;
	public int bulletsPerShot;
	public float timeBetweenShots;
	public float scatter;
	public bool verticalScatter = false;
	public bool uniformScatter = true;
	public float shakeCamIntensity = 0f;
	public AudioClip shotSound;
	public bool cursorBounces = true;
	public int ammoPickup;
	
	public bool canFire {
		get {
			return Time.time - lastShot > timeBetweenShots;
			//return WeaponController.singleton.canFire;
		}
	}
	private float lastShot = -Mathf.Infinity;
	Vector3 initPos;
	//public float reloadTime;
	//public int shotsPerMagazine;
	
	public void Start() {
		ammo = initialAmmo;
		initPos = transform.localPosition;
	}
	
	public void fire() {
		if (!canFire || _ammo <= 0) return;
		var spawn = transform.Find("BulletSpawn");
		for (int i = 0; i < bulletsPerShot; i++) {
			var direction = spawn.rotation;
			var deltaAngle = uniformScatter
				? (float)i/bulletsPerShot * 2 * scatter - scatter
				: Random.Range(-scatter, scatter);
			direction *= Quaternion.AngleAxis(deltaAngle, transform.up);
			if (verticalScatter)
				direction *= Quaternion.AngleAxis(deltaAngle, transform.right);
			var bullet = Instantiate(bulletGO, spawn.position, direction) as GameObject;
			bullet.GetComponent<Bullet>().initBullet(GetComponentInParent<Player>().gameObject, damagerPerBullet);
		}
		ammo--;
		if (shakeCamIntensity != 0f) {
			Globals.shakeCam(shakeCamIntensity);
		}
		if (shotSound)
			Helper.playSound(shotSound, transform);
		lastShot = Time.time;
		WeaponController.singleton.disableShooting(timeBetweenShots);

		// knockback animation
		var weaponKnockback = 1.2f + shakeCamIntensity;
		var returnTime = timeBetweenShots * 0.75f;
		var seq = new Sequence();
		seq.Append(HOTween.To(transform, 0.1f, new TweenParms()
			.Prop("localPosition", initPos - new Vector3(0,0,weaponKnockback))));
		seq.Append(HOTween.To(transform, returnTime, new TweenParms()
			.Prop("localPosition", initPos)));
		seq.Play();
	}
}