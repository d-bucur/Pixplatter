﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Explosion : MonoBehaviour {
	public float damageAmt = 100f;
	public float duration;
	public float explosionForce;

	// Use this for initialization
	void Start () {
		var seq = new Sequence();
		var size = transform.localScale;
		transform.localScale = Vector3.zero;
		seq.Append(HOTween.Punch(transform, 0.4f, "localScale", size));
		seq.Append (
			HOTween.To(transform, duration, new TweenParms()
		           .Prop("localScale", Vector3.zero)
		           .Ease(EaseType.EaseOutQuad))
		);
		seq.Play();
		Globals.shakeCam(0.7f);
	}

	void OnTriggerEnter(Collider coll) {
		var script = coll.GetComponentInParent<Damageable>();
		if (script) {
			var dir = coll.transform.position - transform.position;
			dir.y = 8f - (coll.transform.position - transform.position).magnitude/8f;
			script.damage(damageAmt, coll.transform.rotation, dir.normalized, damageAmt*0.75f);
		}
		var r = coll.GetComponentInParent<Rigidbody>();
		if (coll.gameObject.layer == 10 && r) {
			r.AddExplosionForce(explosionForce, transform.position, 1000f);
		}
	}
}
