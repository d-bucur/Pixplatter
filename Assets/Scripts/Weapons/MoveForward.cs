﻿using UnityEngine;
using System.Collections;

public class MoveForward : MonoBehaviour {
	public float speed;
	public float angle = 0;

	// Use this for initialization
	void Start () {
		var rot = new Vector3(0,0,speed);
		if (angle != 0)
			rot = Quaternion.AngleAxis(angle, Vector3.right) * rot;
		rigidbody.AddRelativeForce(
			rot,
			ForceMode.VelocityChange);
	}
}
