﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class FlameDamager : MonoBehaviour {
	public float deltaTime, damage;
	GameObject flameGO;
	Damageable damageToGO {
		set {
			_damageToGO = value;
			StartCoroutine(damageEveryDelta());
		}
	}
	Damageable _damageToGO = null;

	private IEnumerator damageEveryDelta() {
		yield return new WaitForSeconds(deltaTime);
		_damageToGO.damage(damage);
		StartCoroutine(damageEveryDelta());
	}

	void Awake() {
		flameGO = Resources.Load<GameObject>("FlameDamager");
	}

	void OnTriggerEnter(Collider other) {
		var z = other.gameObject.GetComponentInParent<Zombie>();
		if (z) {
			burn(z.transform.Find("Cube").gameObject);
		}
	}

	void burn(GameObject go) {
		if (go.GetComponentInChildren<FlameDamager>() == null) {
			var flame = Instantiate(flameGO) as GameObject;
			flame.transform.parent = go.transform;
			flame.transform.localPosition = Vector3.zero;
			flame.GetComponent<FlameDamager>().damageToGO = go.GetComponentInParent<Damageable>();
		}
	}
}
