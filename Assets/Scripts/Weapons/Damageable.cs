﻿using UnityEngine;
using System.Collections;
using ObjectExtensions;

public class Damageable : MonoBehaviour {
	public float health = 100f;
	public GameObject deathDebris;
	public float invulnerabilityAfterHit = 0.1f;
	public float initialInvulnerabilityTime = 0;
	public AudioClip soundWhenDamaged;
	public float ragdollLifetime = 0f;

	float lastHitTime = Mathf.NegativeInfinity;

	private float creationTime;

	void Start() {
		creationTime = Time.time;
	}

	public void damage(float amt, Quaternion rot, Vector3 direction, float forceFactor = 10f) {
		if (health <= 0f) return; // no more damage if already dead
		Debug.DrawRay(transform.position, direction*forceFactor/3f, Color.black, 2f);
		if (Time.time - lastHitTime >= invulnerabilityAfterHit) {
			SendMessage("damaged", SendMessageOptions.DontRequireReceiver);
			if (soundWhenDamaged)
				Helper.playSound(soundWhenDamaged, transform);
			lastHitTime = Time.time;
			health -= amt;
			if (health <= 0f) {
				if (Time.time - creationTime > initialInvulnerabilityTime)
					_die(rot, direction, forceFactor);
			}
		}
	}

	public void damage(float amt) {
		damage (amt, Quaternion.identity, new Vector3(Mathf.Infinity,Mathf.Infinity,Mathf.Infinity));
	}
	
	void _die(Quaternion rot, Vector3 direction, float forceFactor) {
		if (deathDebris)
			Instantiate(deathDebris, transform.position + deathDebris.transform.position, rot);
		SendMessage("die", SendMessageOptions.DontRequireReceiver);
		// Prep ragdoll
		var r = transform.Find("ragdoll");
		if (r) {
			Destroy(collider);
			r.gameObject.SetActive(true);
			rigidbody.mass = 1;
			rigidbody.constraints = RigidbodyConstraints.None;
		}
		// add force to ragdoll
		if (rigidbody) {
			if (direction != new Vector3(Mathf.Infinity, Mathf.Infinity, Mathf.Infinity)) {
				Vector3 torqueDir = direction * 10f;
				torqueDir.y = 0;
				torqueDir = Quaternion.Euler(0, 90, 0) * torqueDir;
				Debug.DrawRay(transform.position, torqueDir, Color.red, 2f);
				rigidbody.AddTorque(torqueDir, ForceMode.VelocityChange);
				rigidbody.AddForce(direction * forceFactor, ForceMode.Impulse);
			}
		}

		StartCoroutine(removeBody());
	}

	IEnumerator removeBody() {
		yield return new WaitForSeconds(ragdollLifetime/(4-QualitySettings.GetQualityLevel()));
		var p = GetComponent<Player>();
		if (!p) {
			gameObject.animatedDestroy(0.5f);
		}
	}
}
