﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class BlinkTextColor : MonoBehaviour {

	// Use this for initialization
	void Start () {
		HOTween.To (renderer.material, 0.3f, new TweenParms()
		            .Prop("color", Color.black)
		            .Loops(-1, LoopType.YoyoInverse));
	}
}
