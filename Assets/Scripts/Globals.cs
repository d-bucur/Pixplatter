﻿using UnityEngine;
using System.Collections;

public enum GameState {MainMenu, Playing, Gameover}

public class Globals : MonoBehaviour {
	public static int ammo;
	public static Transform player;
	public static float playerLife;
	public static HUD hud;
	public static CamAnimation camAnimation;
	public static Sunlight sunlight;
	public static int score {
		get {
			return _score;
		}
		set{
			if (gameState == GameState.Playing) {
				_score = value;
				UpdateScore.singleton.updateScore();
			}
		}
	}
	static int _score = 0;
	public static GameState gameState = GameState.MainMenu;
	public static float gameoverTime;
	public static DifficultyHandler difficultyHandler;
	new public static Camera camera;

	public static float difficulty {
		get{
			return difficultyHandler.getDifficulty(Time.timeSinceLevelLoad);
		}
	}


	public static string weaponName {
		set {
			hud.setWeapon(value);
		}
	}

	public static void shakeCam(float intensity = 1f) {
		camAnimation.shakeCam(intensity);
	}

	public static void updatePlayerLife() {
		playerLife = player.GetComponent<Damageable>().health;
	}

	public static void loadMainMenu() {
		gameState = GameState.MainMenu;
		Application.LoadLevel(0);
	}

	public static void gameover() {
		Globals.camera.transform.Find("HUD/CenterAligned/GameOver").gameObject.SetActive(true);
		GameObject.Find ("ScoreLabel").gameObject.GetComponent<TextMesh>().text = Globals.score.ToString();
		gameState = GameState.Gameover;
		gameoverTime = Time.time;
	}

	public static void startPlaying() {
		gameState = GameState.MainMenu;
		_score = 0;
		Application.LoadLevel(Application.loadedLevel);
	}

}