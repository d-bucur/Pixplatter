﻿using UnityEngine;
using System.Collections;


public class ScrollTextInMenu : MonoBehaviour {
	public float copyOffset;
	public float speed;
	float initPos;
	GameObject copy;

	// Use this for initialization
	void Start () {
		initPos = transform.localPosition.x;
		this.enabled = false;
		copy = Instantiate(gameObject) as GameObject;
		copy.transform.parent = transform.parent;
		copy.transform.localPosition = transform.localPosition + new Vector3(copyOffset,0,0);
		copy.transform.localEulerAngles = transform.localEulerAngles;
		Destroy(copy.GetComponent<ScrollTextInMenu>());
		this.enabled = true;
	}

	void FixedUpdate() {
		move(this.gameObject);
		move (copy);
	}

	void move(GameObject go) {
		go.transform.Translate(-speed, 0, 0);
		if (go.transform.localPosition.x < -copyOffset+initPos){
			var t = go.transform.localPosition;
			t.x = initPos + copyOffset;
			go.transform.localPosition = t;
		}
	}
}
