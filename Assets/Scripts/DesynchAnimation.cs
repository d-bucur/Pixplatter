﻿using UnityEngine;
using System.Collections;

public class DesynchAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		foreach (var a in GetComponentsInChildren<Animator>()) {
			a.speed = Random.Range(1.1f, 0.8f);
		}
	}
}
