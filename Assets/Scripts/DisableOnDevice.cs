﻿using UnityEngine;
using System.Collections;

public class DisableOnDevice : MonoBehaviour {
	public bool disableOnMobile = true;
	public bool disableOnPC = false;

	void Awake () {
		if (disableOnMobile && Application.platform == RuntimePlatform.Android)
			Destroy(gameObject);
		if (disableOnPC && (Application.platform == RuntimePlatform.WindowsPlayer
			|| Application.platform == RuntimePlatform.WindowsEditor
			|| Application.platform == RuntimePlatform.WindowsWebPlayer))
			Destroy(gameObject);
	}
}
