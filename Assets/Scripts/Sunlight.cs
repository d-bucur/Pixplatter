﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;

public class Sunlight : MonoBehaviour {
	List<Light> lamps = new List<Light>();
	bool on = false;
	float initIntensity;

	void Awake() {
		Globals.sunlight = this;
	}

	public void addLight(Light l) {
		lamps.Add(l);
		initIntensity = l.intensity;
	}

	public void changeLights() {
		on = !on;
		foreach(var l in lamps) {
			tweenLight(l);
		}
	}

	void tweenLight(Light l) {
		float newIntensity = l.enabled ? 0 : initIntensity;
		HOTween.To(l, 4f, new TweenParms()
		           .Prop("intensity", newIntensity)
		           .OnComplete(() => l.enabled = on));
	}
}
