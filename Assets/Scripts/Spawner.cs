﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour, ReactToPlayer {
	public GameObject item;
	public float frequency;
	public float desync = 0.2f;

	void Start() {
		StartCoroutine("timedSpawn");
	}

	public void enterRange() {
		StartCoroutine ("timedSpawn");
	}

	public void exitRange() {
		StopCoroutine("timedSpawn");
	}
	
	IEnumerator timedSpawn() {
		float waitTime = frequency+Random.Range(-desync, desync);
		waitTime /= Globals.difficulty;
		yield return new WaitForSeconds(waitTime);

		// raycast underneath spawn point
		if (Physics.Raycast(transform.position, Vector3.down, Mathf.Infinity, 1 << 10)) {
			Instantiate(item, transform.position, Quaternion.identity);
			/*var e = Instantiate(Resources.Load("TeleportEffect")) as GameObject;
			e.transform.position = transform.position;*/
		}

		StartCoroutine ("timedSpawn");
	}
}
