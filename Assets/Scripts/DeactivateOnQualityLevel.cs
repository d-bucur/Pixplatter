﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DeactivateOnQualityLevel : MonoBehaviour {
	public List<Component> components;
	public List<int> deactivationLevels;
	
	void Awake () {
		for (int i = 0; i < components.Count; i++) {
			if (QualitySettings.GetQualityLevel() <= deactivationLevels[i])
				if (components[i].GetType() == typeof(Transform)) {
					Destroy(components[i].gameObject);
				}
				else {
					Destroy(components[i]);
				}
		}
		Destroy(this);
	}
}
