﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class FloatingText : MonoBehaviour {
	public string text {
		get {
			return GetComponent<TextMesh>().text;
		}
		set{
			foreach(Transform t in transform) {
				t.GetComponent<TextMesh>().text = value;
			}
		}
	}
	public float duration;
	public float size;

	// Use this for initialization
	void Start () {
		HOTween.To(transform, duration, "position", new Vector3(0,5,0), true);
		var seq = new Sequence();
		seq.Append(HOTween.To(transform, duration*0.75f, new TweenParms().Prop("localScale", new Vector3(1,1,1)*size).Ease(EaseType.EaseOutBounce)));
		seq.Append(HOTween.To(transform, duration*0.25f, new TweenParms().Prop("localScale", Vector3.zero)));
		seq.Play();
		Destroy (gameObject, duration);
	}
}
