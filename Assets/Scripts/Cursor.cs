﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Cursor : MonoBehaviour {
	public static Cursor singleton {
		get {
			return _singleton;
		}
	}
	public static Cursor _singleton;
	public GameObject cursor;
	public bool placeInWorld = false;

	void Awake() {
		_singleton = this;
	}
	
	// Update is called once per frame
	void Update () {
		if (placeInWorld) {
			RaycastHit hitInfo = new RaycastHit();
			if (Physics.Raycast(transform.position, transform.forward, out hitInfo, 100f, 1 << 10 | 1 << 9)) {
				var newPos = hitInfo.point + (transform.position - hitInfo.point).normalized * 2f;
				newPos.y = 1f;
				HOTween.To(cursor.transform, 0.2f, new TweenParms()
					.Prop("position", newPos));
			}
		}
	}

	public void fade(bool visible) {
		if (visible) {
			var seq = new Sequence();
			seq.Append(HOTween.To(cursor.transform, 0.35f, new TweenParms()
				.Prop("localScale", new Vector3(1.5f, 1.5f, 1.5f))
				.Ease(EaseType.EaseOutBounce)));
			seq.Append(HOTween.To(cursor.transform, 0.35f, new TweenParms()
				.Prop("localScale", new Vector3(1, 1, 1))
				.Ease(EaseType.EaseInSine)));
			seq.Play();
		}
		else
			HOTween.To(cursor.transform, 0.5f, new TweenParms()
				.Prop("localScale", Vector3.zero)
				.Ease(EaseType.EaseInSine));
	}
}
