﻿using UnityEngine;
using System.Collections;

public class DestroyPS : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Destroy(gameObject, Mathf.Min(particleSystem.duration, particleSystem.startLifetime));
	}
}
