﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DebugControls : MonoBehaviour {
	PixellationEffect pixEffect;
	List<int> resolutionsList = new List<int>(new int[] {32, 64, 128});
	int rezPointer = 0;

	void Start() {
		pixEffect = Globals.camera.GetComponent<PixellationEffect>();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.P)) {
			pixEffect.postProcess = !pixEffect.postProcess;
			Debug.Log("Pixellation: " + (pixEffect.postProcess ? "ON" : "OFF"));
		}
		if (Input.GetKeyDown(KeyCode.L)) {
			changeResolution();
			Debug.Log("Screen resolution: " + resolutionsList[rezPointer]);
		}
	}

	void changeResolution() {
		rezPointer = ((rezPointer - 1) + resolutionsList.Count ) % resolutionsList.Count;
		camera.targetTexture.Release();
		camera.targetTexture = RenderTexture.GetTemporary(resolutionsList[rezPointer], resolutionsList[rezPointer]);
		camera.targetTexture.filterMode = FilterMode.Point;
		//camera.targetTexture.width = (camera.targetTexture.height = resolutionsList[rezPointer]);
	}
}
