﻿using UnityEngine;
using System.Collections;

[AddComponentMenu("Camera-Control/Mouse Orbit with zoom")]
public class MouseOrbit : MonoBehaviour {
	
	public Transform target;
	public float distance = 5.0f;
	public float xSpeed = 120.0f;
	public float ySpeed = 120.0f;
	public float distanceMin = .5f;
	public float distanceMax = 15f;
	
	float x = 0.0f;
	public float y = 0.0f;
	
	// Use this for initialization
	void Start () {
		Vector3 angles = new Vector3(45,0,0);
		x = angles.y;
	}
	
	void LateUpdate () {
		Quaternion rot;
		Vector3 pos;
		getPos(out pos, out rot);
		transform.position = pos;
		transform.rotation = rot;
	}

	public void offsetX(float val) {
		x += val * xSpeed * distance * 0.02f;
	}

	public void getPos(out Vector3 pos, out Quaternion rot) {

		Quaternion rotation = Quaternion.Euler(y, x, 0);

		Vector3 negDistance = new Vector3(0.0f, 0.0f, -distance);
		Vector3 position = rotation * negDistance + target.position;

		rot = rotation;
		pos = position;
	}
	
	public static float ClampAngle(float angle, float min, float max)
	{
		if (angle < -360F)
			angle += 360F;
		if (angle > 360F)
			angle -= 360F;
		return Mathf.Clamp(angle, min, max);
	}
	
	
}