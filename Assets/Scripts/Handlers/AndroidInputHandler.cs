﻿using UnityEngine;
using System.Collections;

public class AndroidInputHandler : MovementInput {
	// TODO refactor this shit... seriously
	Joystick leftJoystick, rightJoystick;
	int leftTouch, rightTouch;
	Vector2 leftDelta, rightDelta, leftInitPosition, rightInitPosition;

	public float shootingButtonHeight;
	public float weaponChangeThreshold;
	public float rotationSensitivity, triggerSensititvity;
	public float maxLeftSensitivityPercentage, maxRightSensitivityPercentage;
	float maxLeftPixelSensititvity, maxRightPixelSensititvity, triggerPixelSensititvity;

	int isShootingId = -1;

#if UNITY_ANDROID

	override protected void Awake() {
		base.Awake();
		maxRightPixelSensititvity = Screen.width * maxRightSensitivityPercentage;
		maxLeftPixelSensititvity = Screen.width * maxLeftSensitivityPercentage;
		triggerPixelSensititvity = Screen.height * triggerSensititvity;
	}
	
	// Update is called once per frame
	void Update() {
		touchHandler();
		rotateCameraToCursor();
		// apply movement
		dir = Vector3.ClampMagnitude(new Vector3(leftDelta.x, 0, leftDelta.y), maxLeftPixelSensititvity) / maxLeftPixelSensititvity;
		dir.x *= Mathf.Sign(dir.x) * dir.x;
		dir.y *= Mathf.Sign(dir.y) * dir.y;

		// apply rotation
		/*old rotation type
		 * var rotationDir = Mathf.Clamp(rightDelta.x, -maxRightPixelSensititvity, maxRightPixelSensititvity) / maxRightPixelSensititvity;
		rotationDir *= Mathf.Sign(rotationDir) * rotationDir;	//exponential falloff*/
		//var rotationDir = Mathf.Sign(rightDelta.x) * rightDelta.x * rightDelta.x;

		// new rotation
		/*var viewportDelta = Camera.main.ScreenToViewportPoint(rightDelta);
		if (viewportDelta != null)
			orbitScript.offsetX(viewportDelta.x * rotationSensitivity);*/

		var v = rightDelta.x / (float)Screen.width * rotationSensitivity;
		orbitScript.offsetX(v);

		/* old shooting method
		if (rightDelta.y > triggerPixelSensititvity)
			WeaponController.singleton.fire();
		 */
		if (isShootingId != -1)
			WeaponController.singleton.fire();
		
	}

	void touchHandler() {
		for (int i = 0; i < Input.touchCount; i++) {
			var t = Input.GetTouch(i);
			// register new touches
			if (t.phase == TouchPhase.Began) {
				if (t.position.x < Screen.width / 2f) {
					leftTouch = t.fingerId;
					leftInitPosition = t.position;
					// register weapon change
					if (t.position.y > Screen.height * weaponChangeThreshold)
						WeaponController.singleton.getWeapon(false);
				}
				else {
					rightTouch = t.fingerId;
					rightInitPosition = t.position;
					// register weapon change
					if (t.position.y > Screen.height * weaponChangeThreshold)
						WeaponController.singleton.getWeapon(true);
					// register shooting button press
					//if (buttonRect.Contains(Camera.main.ScreenToViewportPoint(t.position))) {
					if (t.position.y < Screen.height * shootingButtonHeight) {
						isShootingId = t.fingerId;
					}
				}
			}
			// unregister expired touches
			if (t.phase == TouchPhase.Ended) {
				if (t.fingerId == leftTouch) {
					leftTouch = -1;
					leftDelta = Vector2.zero;
				}
				else if (t.fingerId == rightTouch) {
					rightTouch = -1;
					rightDelta = Vector2.zero;
				}
				if (t.fingerId == isShootingId) {
					isShootingId = -1;
				}
			}
			// handle old touches
			if (t.fingerId == leftTouch) {
				leftDelta = t.position - leftInitPosition;
			}
			else if (t.fingerId == rightTouch) {
				//rightDelta = t.position - rightInitPosition; // delta from touch start point
				rightDelta = t.deltaPosition / t.deltaTime;	// delta movement normalized by time
				// solves weird bug on Android
				if (float.IsNaN(rightDelta.x))
					rightDelta.x = 0f;
				if (float.IsNaN(rightDelta.y))
					rightDelta.y = 0f;
			}
		}
	}
#endif
}