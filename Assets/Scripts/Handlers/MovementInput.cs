﻿using UnityEngine;
using System.Collections;

public class MovementInput : MonoBehaviour {
	public float acceleration;
	public float jumpForce;
	protected MouseOrbit orbitScript;

	protected Vector3 dir;
	bool jump;

	virtual protected void Awake() {
		orbitScript = GameObject.Find("CameraHolder").GetComponent<MouseOrbit>();
	}

	protected void rotateCameraToCursor() {
		var t = transform.localEulerAngles;
		t.y = Globals.camera.transform.eulerAngles.y;
		transform.localEulerAngles = t;
	}

	// Update is called once per frame
	void Update () {
		rotateCameraToCursor();
		dir = new Vector3(Input.GetAxisRaw ("Horizontal"), 0,Input.GetAxisRaw ("Vertical")).normalized;
		//if (Input.GetKeyDown(KeyCode.Space)) jump = true;
		orbitScript.offsetX(Input.GetAxis("Mouse X"));
	}

	void FixedUpdate() {
		var forwardNormalized = Globals.camera.transform.forward;
		forwardNormalized.y = 0;
		forwardNormalized.Normalize();
		rigidbody.AddForce (forwardNormalized * dir.z * acceleration, ForceMode.Acceleration);
		rigidbody.AddForce (Globals.camera.transform.right * dir.x * acceleration, ForceMode.Acceleration);
		if (jump) {
			rigidbody.AddExplosionForce(jumpForce, transform.position + new Vector3(0,-0.1f,0), 100f);
			//rigidbody.AddForce(Vector3.up * jumpForce, ForceMode.VelocityChange);
			jump = false;
		}
	}
}
