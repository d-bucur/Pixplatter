﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameHandler : MonoBehaviour {
	public GameObject pausedHUD;
	public MouseOrbit orbitScript;
	public static bool isPaused;
	public static GameHandler singleton;

	// Use this for initialization
	void Awake () {
		isPaused = true;
		singleton = this;
		orbitScript.enabled = false;

		Time.timeScale = 0f;
		pausedHUD.SetActive(true);
	}

	void Update() {
		if (!Input.GetKeyDown(KeyCode.Escape) && isPaused && Input.anyKeyDown) {
			pause();
		}
	}

	public void pause(bool animate = true) {
		isPaused = !isPaused;
#if !UNITY_ANDROID
		if (isPaused)
			orbitScript.enabled = false ;
#endif

		Time.timeScale = isPaused ? 0 : DifficultyHandler.getTimeScale();
		pausedHUD.SetActive(isPaused);
		Globals.gameState = !isPaused ? GameState.Playing: GameState.MainMenu;

		// animate camera to position
		if (animate) {
			Quaternion rot;
			Vector3 pos;
			orbitScript.getPos(out pos, out rot);
			HOTween.To(orbitScript.transform, 1f, new TweenParms()
				.Prop("position", pos));
			HOTween.To(orbitScript.transform, 1f, new TweenParms()
				.Prop("rotation", rot)
				.OnComplete(() => orbitScript.enabled = !isPaused));
		}
	}
}
