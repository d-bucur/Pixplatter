﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class HUD : MonoBehaviour {
	public bool lockCursor = true;
	TextMesh weaponLabel;
	Sequence weaponTween;
	RaycastHit hitInfo;
	float disableClicksTime = 2f;

	const float weaponLabelStart = 0.2744313f;
	const float weaponLabelEnd = 0.1796125f;

	// Use this for initialization
	void Start () {
		Globals.hud = this;
		var wl = GameObject.Find("WeaponLabel");
		if (wl)
			weaponLabel = wl.GetComponent<TextMesh>();

		Screen.lockCursor = lockCursor;
	}
	
	public void setWeapon(string name) {
		/*HOTween.To(weaponLabel, 0.5f, new TweenParms()
		           .Prop("text", name));*/
		if (weaponTween != null)
			weaponTween.Kill();
		var pos = weaponLabel.transform.localPosition;
		pos.x = weaponLabelStart;
		weaponLabel.transform.localPosition = pos;
		pos.x = weaponLabelEnd;
		weaponLabel.text = name;

		weaponTween = new Sequence();
		weaponLabel.transform.localScale = new Vector3(0.5f, 0, 1);
		HOTween.To(weaponLabel.transform, 0.5f, new TweenParms()
		           .Prop("localScale", new Vector3(1,1,1))
		           .Ease(EaseType.EaseOutSine));
		weaponTween.Append(HOTween.To(weaponLabel.transform, 0.7f, new TweenParms()
		           .Prop("localPosition", pos)
		           .Ease(EaseType.EaseOutBounce)));
		weaponTween.Play();
	}

	void Update() {
		if (Input.GetKeyDown(KeyCode.Escape))
			GameHandler.singleton.pause(false);
		if (Input.GetMouseButtonDown(0) && Globals.gameState == GameState.Gameover && Time.time - Globals.gameoverTime > disableClicksTime)
			Globals.startPlaying();
		if (Input.GetMouseButtonDown(0)) {
			Ray r = Globals.camera.ScreenPointToRay(Input.mousePosition);
			if (Physics.Raycast(r, out hitInfo, Mathf.Infinity, 1<<14)) {
				hitInfo.collider.GetComponent<HUDButton>().clicked();
			}
		}
	}
}
