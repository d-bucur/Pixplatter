﻿using UnityEngine;
using System.Collections;

public class DifficultyHandler : MonoBehaviour {
	public AnimationCurve difficulty;

	public float getDifficulty(float time) {
		print(difficulty.Evaluate(time));
#if UNITY_ANDROID
		return difficulty.Evaluate(time)*0.75f;
#else
		return difficulty.Evaluate(time);
#endif
	}

	static public float getTimeScale() {
#if UNITY_ANDROID
		return 0.85f;
#else
		return 1f;
#endif
	}
	void Awake() {
		Globals.difficultyHandler = this;
	}

}
