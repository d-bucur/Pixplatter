﻿using UnityEngine;
using System.Collections;

public class UpdateScore : MonoBehaviour {
	public static UpdateScore singleton;
	public int deltaTime;
	public int scorePerDeltaTime;

	private bool textIs3D;

	// Use this for initialization
	void Start () {
		textIs3D = GetComponent<GUIText>() == null;
		singleton = this;
		updateScore();
		StartCoroutine("addSurvivedScore");
	}

	public void updateScore() {
		if (textIs3D)
			foreach (var l in GetComponentsInChildren<TextMesh>())
				l.text = Globals.score.ToString();
		else
			foreach (var l in GetComponentsInChildren<GUIText>())
				l.text = Globals.score.ToString();
	}

	IEnumerator addSurvivedScore() {
		yield return new WaitForSeconds(deltaTime);
		Globals.score += scorePerDeltaTime;
		StartCoroutine("addSurvivedScore");
	}
}