﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

namespace ObjectExtensions {
	public static class ObjectExtensionsClass {
		public static void animatedDestroy(this GameObject go, float time = 1f) {
			if (go.collider != null) {
				GameObject.Destroy(go.collider);
			}
			if (go.rigidbody != null) {
				GameObject.Destroy(go.rigidbody);
			}
			
			foreach (var c in go.GetComponents<MonoBehaviour>()) {
				GameObject.Destroy(c);
			}

			if (go.isStatic)
				GameObject.Destroy(go);
			else
				HOTween.To(go.transform, time, new TweenParms()
			           .Prop("localScale", Vector3.zero)
			           .Ease(EaseType.EaseInOutQuad)
			           .OnComplete(()=>Object.Destroy(go)));

		}
	}
}
