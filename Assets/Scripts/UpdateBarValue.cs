﻿using UnityEngine;
using System.Collections;
using System.Reflection;
using Holoville.HOTween;

public class UpdateBarValue : MonoBehaviour {
	// Scales the bar (0,1 on x) to match the field in globals given
	public string fieldName;
	float initVal, initBarScale;
	FieldInfo fieldInfo;
	float lastVal;

	float getVal() {
		return (float)fieldInfo.GetValue(null);
	}

	void Start() {
		// Prepare field info for reflection
		fieldInfo = typeof(Globals).GetField(fieldName);
		// Get initial value of the field
		// This will correspond to scale 1
		initVal = getVal();
		initBarScale = transform.localScale.x;
		HOTween.Init(false, false, true);
		HOTween.EnableOverwriteManager();
	}

	// TODO does not work with properties
	void FixedUpdate() {
		var s = transform.localScale;
		if (getVal() != lastVal) {
			// Update only when it has changed
			s.x = getVal()/initVal * initBarScale;
			HOTween.To(transform, 0.3f, new TweenParms()
			           .Prop("localScale", s)
			           .Ease(EaseType.EaseOutQuad));
			//transform.localScale = s;
			lastVal = getVal();
		}
	}
}
