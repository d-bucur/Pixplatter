﻿using UnityEngine;
using System.Collections;

public class Charger : Zombie {
	public float chargeForce;
	public float prepTime;
	public float rechargeTime;
	public float chargeDistanceSquared;
	
	bool canCharge = true;

	// Update is called once per frame
	void FixedUpdate () {
		if (playerInRange && canCharge) {
			var distance = (Globals.player.position - transform.position).sqrMagnitude;
			if (distance < chargeDistanceSquared)
				if (!Physics.Raycast(transform.position, Globals.player.position, Mathf.Infinity, 1<<10)) {
					StartCoroutine(charge());
				}
		}
	}

	protected  void OnCollisionEnter(Collision c) {
		if (!canCharge) {
			var s = c.gameObject.GetComponent<Damageable>();
			if (!s) return;
			var charger = c.gameObject.GetComponent<Charger>();
			if (charger) return;
			var p = c.gameObject.GetComponent<Player>();
			s.damage(p ? damage : 100f);
		}
	}

	IEnumerator charge() {
		var playerDir = transform.position - Globals.player.position;
		playerDir.y = 0f;
		canCharge = (rigidbody.isKinematic = (navAgent.enabled = false));
		GetComponent<Animator>().SetTrigger("PrepCharge");
		yield return new WaitForSeconds(prepTime);
		rigidbody.AddExplosionForce(chargeForce, transform.position +
		       	playerDir.normalized*10f, 100f, 0, ForceMode.Acceleration);
		yield return new WaitForSeconds(rechargeTime);
		canCharge = (navAgent.enabled = (rigidbody.isKinematic = true));
	}
}
