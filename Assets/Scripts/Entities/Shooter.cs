﻿using UnityEngine;
using System.Collections;

public class Shooter : Zombie {
	public float range;
	public float rechargeTime;
	public GameObject projectile;

	IEnumerator shoot() {
		yield return new WaitForSeconds(rechargeTime);
		if (!Physics.Raycast(transform.position, Globals.player.position, Mathf.Infinity, 1<<10)) {
			var dir = Globals.player.position-transform.position;
			dir.y = 0;
			var go = Instantiate(projectile, transform.position + projectile.transform.position + dir.normalized * 2f, Quaternion.identity) as GameObject;
			go.GetComponent<Bullet>().initBullet(gameObject, damage);
			go.transform.rotation = Quaternion.LookRotation(dir);
		}
		StartCoroutine (shoot ());
	}

	override public void enterRange() {
		playerInRange = true;
		StartCoroutine("followPlayer", range);
		StartCoroutine("shoot");
	}
	
	override public void exitRange() {
		playerInRange = false;
		StopCoroutine("followPlayer");
		StopCoroutine("shoot");
	}
}
