﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class Player : MonoBehaviour {
	float hitPunch = 30f;
	public GameObject ouchEffect;

	void Awake() {
		Globals.player = transform;
		Globals.updatePlayerLife();
		HOTween.Init(false, false, true);
		HOTween.EnableOverwriteManager(false);
	}

	public void damaged() {
		Globals.updatePlayerLife();
		HOTween.Shake(Globals.camera, 0.35f, new TweenParms()
		              .Prop ("fieldOfView", Globals.camera.fieldOfView+hitPunch));
		var seq = new Sequence();
		seq.Append(HOTween.To(ouchEffect.transform, 0.2f, new TweenParms()
			.Prop("localPosition", new Vector3(0, 0, 1.15f))));
		seq.Append(HOTween.To(ouchEffect.transform, 0.1f, new TweenParms()
			.Prop("localPosition", new Vector3(0, 0, -0.01f))));
		seq.Play();
	}

	public void die() {
		foreach (var c in GetComponents<MonoBehaviour>()) {
			Destroy(c);
		}
		transform.Find("Model").gameObject.SetActive(false);
		Globals.gameover();
	}
}
