﻿using UnityEngine;
using System.Collections;
using ObjectExtensions;

public class Zombie : MonoBehaviour, ReactToPlayer {
	public float damage;
	float updateTime = 0.75f;
	protected NavMeshAgent navAgent;
	protected bool playerInRange = false;
	public int scoreAddedOnKill = 10;

	// Use this for initialization
	void Start () {
		navAgent = GetComponent<NavMeshAgent>();
	}
	
	protected IEnumerator followPlayer(float range) {
		if (Globals.player && navAgent.enabled)
			navAgent.SetDestination(Globals.player.position + range * (transform.position - Globals.player.position).normalized);
		yield return new WaitForSeconds(updateTime);
		StartCoroutine("followPlayer", range);
	}

	protected void OnCollisionStay(Collision c) {
		var p = c.gameObject.GetComponent<Player>();
		if (p)
			p.GetComponent<Damageable>().damage(damage);
	}

	virtual public void enterRange() {
		playerInRange = true;
		StartCoroutine("followPlayer", 0f);
	}

	virtual public void exitRange() {
		playerInRange = false;
		StopCoroutine("followPlayer");
	}

	virtual public void die() {
		// floating score text
		Helper.createFloatingText(transform.position, scoreAddedOnKill.ToString());

		Globals.score += scoreAddedOnKill;
		GetComponent<Animator>().SetTrigger("Die");
		Destroy(GetComponent<NavMeshAgent>());
		rigidbody.isKinematic = false;
		Destroy(this);
	}
}
