﻿using UnityEngine;
using System.Collections;

public class SelfDestructAudio : MonoBehaviour {
	public string clipName {get;set;}

	public void playClip(AudioClip clip) {
		audio.clip = clip;
		audio.Play();
		Destroy(gameObject, audio.clip.length);
	}
}
