﻿using UnityEngine;
using System.Collections;

public class Helper : MonoBehaviour {
	public static int playerHealth, ammo;
	public static string weaponName, powerName;
	
	public static Transform getFirstComponentInParents<C>(Transform t) where C : Component {
		if (t.GetComponent<C>())
			return t;
		else
			return getFirstComponentInParents<C>(t.parent);
	}
	
	public static void playSound(AudioClip clip, Transform pos = null, bool follow = false, int? priority = null, float? volume = null) {
		if (clip == null) return;
		var o = (GameObject)Instantiate(Resources.Load ("SelfDestructAudio"),
		                                pos == null ? Globals.camera.transform.position : pos.position ,
		                                Quaternion.identity);
		o.GetComponent<SelfDestructAudio>().playClip(clip);
	}


	public static void createFloatingText(Vector3 pos, string text) {
		createFloatingText(pos, text, Color.white);
	}

	public static void createFloatingText(Vector3 pos, string text, Color color) {
		var v = (GameObject)Instantiate((GameObject)Resources.Load("FloatingText"));
		v.transform.position += pos;
		v.GetComponent<FloatingText>().text = text;
	}
}