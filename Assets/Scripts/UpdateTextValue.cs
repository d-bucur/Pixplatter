﻿using UnityEngine;
using System.Collections;
using System.Reflection;

public class UpdateTextValue : MonoBehaviour {
	// Updates value of the text to the given field in Globals
	public string fieldName;

	// TODO does not work with properties
	void FixedUpdate() {
		FieldInfo info = typeof(Globals).GetField(fieldName);
		var v = info.GetValue(null);
		foreach(var t in GetComponentsInChildren<TextMesh>()) {
			t.text = v.ToString();
		}
	}
}
