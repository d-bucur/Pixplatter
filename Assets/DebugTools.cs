﻿using UnityEngine;
using System.Collections;

public class DebugTools : MonoBehaviour {
	void Update() {
		if (Input.GetKeyDown(KeyCode.P)) {
			var contrastScript = Camera.main.GetComponent<ContrastStretchEffect>();
			if (contrastScript) {
				contrastScript.enabled = !contrastScript.enabled;
			}
		}
	}
}
